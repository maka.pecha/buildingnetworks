import firebaseService from 'app/services/firebaseService';
import jwtService from 'app/services/jwtService';
import {setUserData} from './user.actions';
import * as Actions from 'app/store/actions';
import { auth } from 'firebase/app'; // plugin de firebase
import * as UserActions from './user.actions';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export function submitLogin({email, password})
{
    return (dispatch) =>
        jwtService
          .signInWithEmailAndPassword(email, password)
          .then(user => {
            dispatch(UserActions.setUserData(user));
            window.location.pathname = '/';
            return dispatch({
              type: LOGIN_SUCCESS
            });
          })
          .catch(error => {
            dispatch(Actions.showMessage({message: error}));
              return dispatch({
                  type   : LOGIN_ERROR,
                  payload: error
              });
          });
}

export function submitLoginWithFireBase({username, password})
{
    if ( !firebaseService.auth )
    {
        console.warn("Firebase Service didn't initialize, check your configuration");

        return () => false;
    }

    return (dispatch) =>
        firebaseService.auth.signInWithEmailAndPassword(username, password)
            .then(() => {
                return dispatch({
                    type: LOGIN_SUCCESS
                });
            })
            .catch(error => {
                console.info('error')
                const usernameErrorCodes = [
                    'auth/email-already-in-use',
                    'auth/invalid-email',
                    'auth/operation-not-allowed',
                    'auth/user-not-found',
                    'auth/user-disabled',
                    'auth/account-exists-with-different-credential'
                ];
                const passwordErrorCodes = [
                    'auth/weak-password',
                    'auth/wrong-password'
                ];

              if (error.code === 'auth/user-not-found') {
                error.message =
                  'No hay registro de usuario correspondiente a este identificador. El usuario puede haber sido eliminado';
              } else if (error.code === 'auth/email-already-in-use') {
                error.message = 'El email ya está en uso';
              } else if (error.code === 'auth/invalid-email') {
                error.message = 'La dirección de correo electrónico está mal escrita';
              } else if (error.code === 'auth/operation-not-allowed') {
                error.message = 'Operación no permitida';
              } else if (error.code === 'auth/user-disabled') {
                error.message = 'La cuenta de usuario ha sido deshabilitada por un administrador';
              } else if (error.code === 'auth/account-exists-with-different-credential') {
                error.message =
                  'Ya existe una cuenta con la misma dirección de correo electrónico pero con credenciales de inicio de sesión diferentes. Inicie sesión con un proveedor asociado con esta dirección de correo electrónico';
              } else if (error.code === 'auth/weak-password') {
                error.message = 'La contraseña debe tener al menos 6 caracteres';
              } else if (error.code === 'auth/wrong-password') {
                error.message = 'La contraseña no es válida o el usuario no tiene una contraseña';
              }

                const response = {
                    username: usernameErrorCodes.includes(error.code) ? error.message : null,
                    password: passwordErrorCodes.includes(error.code) ? error.message : null
                };

                if ( error.code === 'auth/invalid-api-key' )
                {
                    dispatch(Actions.showMessage({message: error.message}));
                }

                return dispatch({
                    type   : LOGIN_ERROR,
                    payload: response
                });
            });
}
