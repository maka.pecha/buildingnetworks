const navigationConfig = [
    {
        'id'      : 'applications',
        'title'   : 'Aplicaciones',
        'type'    : 'group',
        'icon'    : 'apps',
        'children': [
            {
                'id'   : 'inicio-component',
                'title': 'Inicio',
                'type' : 'item',
                'icon' : 'home',
                'url'  : '/inicio'
            },
            {
                'id'   : 'tickets-component',
                'title': 'Tickets',
                'type' : 'item',
                'icon' : 'note',
                'url'  : '/tickets'
            },
            {
                'id'   : 'usuarios-component',
                'title': 'Usuarios',
                'type' : 'item',
                'icon' : 'account_circle',
                'url'  : '/usuarios',
            },
            {
                'id'   : 'ajustes-component',
                'title': 'Ajustes',
                'type' : 'item',
                'icon' : 'settings',
                'url'  : '/ajustes'
            }
        ]
    }
];

export default navigationConfig;
