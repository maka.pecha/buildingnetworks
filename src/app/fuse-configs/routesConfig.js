import React from 'react';
import {Redirect} from 'react-router-dom';
import {FuseUtils} from '@fuse';
import {InicioConfig} from "app/main/Inicio/InicioConfig";
import {TicketsConfig} from "app/main/Tickets/TicketsConfig";
import {UsuariosConfig} from "app/main/Usuarios/UsuariosConfig";
import {AjustesConfig} from "app/main/Ajustes/AjustesConfig";
import {LoginConfig} from "app/main/login/LoginConfig";
import {pagesConfigs} from 'app/main/pages/pagesConfigs';
import {NewUserConfig} from "app/main/Usuarios/NewUser/NewUserConfig";

const routeConfigs = [
    InicioConfig,
    TicketsConfig,
    UsuariosConfig,
    AjustesConfig,
    LoginConfig,
    ...pagesConfigs,
    NewUserConfig,
];

const routes = [
    ...FuseUtils.generateRoutesFromConfigs(routeConfigs, null),
    {
        path     : '/',
        component: () => <Redirect to="/inicio"/>
    },
    {
        path     : '/login',
        component: () => <Redirect to="/login"/>
    },
    {
        path     : '/tickets',
        component: () => <Redirect to="/tickets"/>
    },
    {
        path     : '/usuarios',
        component: () => <Redirect to="/usuarios"/>
    },
    {
        path     : '/newUser',
        component: () => <Redirect to="/newUser"/>
    },
    {
        path     : '/ajustes',
        component: () => <Redirect to="/ajustes"/>
    },
];

export default routes;
