import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import SimpleCardContent from './Components/CardContent';
import {Typography} from "@material-ui/core";

const styles = theme => ({
  layoutRoot: {},
  title: {
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20
  },
  back: {
    marginBottom: 6,
    fontSize: 18,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  pos: {
    fontSize: 18,
    margin: theme.spacing(1.2),
    color: '#a7dae6',
  },
});

class Ajustes extends Component {
  constructor(props){
    super(props);
    this.state = {
      entornos : [],
      compania: {},
      entornoId : localStorage.getItem('id_entorno') || 1,
    };
  }

  componentDidMount(){
    this.FetchEntornos();
    this.FetchCompañia();
  }

  saveEntorno = event => {
    const entornoid =  event.target.value;
    this.setState({entornoId: entornoid});
    localStorage.setItem('id_entorno', entornoid);
  };

  FetchCompañia(){
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/compania", requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({compania : result});
      })
      .catch(error => console.log('error', error));
  }

  FetchEntornos(){
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/entornos", requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({entornos : result});
      })
      .catch(error => console.log('error', error))
  }

  render()
  {
    const {classes} = this.props;
    return (
      <FusePageSimple
        classes={{
          root: classes.layoutRoot
        }}
        header={
          <Typography className={classes.title}>
           Ajustes
          </Typography>
        }
        content={
          <div className="p-24">
            <br/>
              <SimpleCardContent
                entornos={this.state.entornos}
                entornoId={this.state.entornoId}
                compania={this.state.compania}
                saveEntorno={this.saveEntorno}
              />
            <br/>
          </div>
        }
      />
    )
  }

}

export default withStyles(styles, {withTheme: true})(Ajustes);