import Ajustes from './Ajustes';

export const AjustesConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
    {
        path     : '/ajustes',
        component: Ajustes
    }
    ]
};

