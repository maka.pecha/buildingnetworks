import React, {Component, useState} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import MenuItem from '@material-ui/core/MenuItem';
import ListItemText from '@material-ui/core/ListItemText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ModalGuardar from './Modal'
import Modal from '@material-ui/core/Modal';

const styles = theme => ({
  layoutRoot: {},
  card: {
    width: '100%',
    minWidth: 100,
    backgroundColor: '#F1F3F9',
    marginBottom: 25,
    fontSize: 20,
    padding: 10,
  },
  cardHeader: {
    padding: '30px',
    borderRadius: 20,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 6,
    fontSize: 18,
  },
  textField: {
    width: 200,
  },
  margin: {
    margin: theme.spacing(1),
  },
  header: {
    color: '#172a3a',
    fontSize: 22,
  },
  formControl: {
    minWidth: 120,
    backgroundColor: '#f1f3f9',
    width: '100%',
    padding: 10,
  },
});

class SimpleCardContent extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
  }

  render () {
    const {classes, entornos, saveEntorno, compania, entornoId} = this.props;

    return(
        <div>
          <Card className={classes.cardHeader}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Typography className={classes.header} style={{paddingBlockEnd: 25}}>
                Datos de Entorno
              </Typography>
            </Grid>
            <Grid container spacing={5}>
              <Grid item xs={12} sm={6}>
                <Typography className={classes.header}>
                  Entorno
                </Typography>
                <Card className={classes.card} color="textSecondary" style={{padding: 0}}>
                  <FormControl className={classes.formControl}>
                    <ModalGuardar entornos={entornos} entornoId={entornoId} saveEntorno={saveEntorno}/>
                  </FormControl>
                </Card>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography className={classes.header}>
                  Email de la compañia
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {compania.email || '-'}
                </Card>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography className={classes.header}>
                  Web
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {compania.web || '-'}
                </Card>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Typography className={classes.header}>
                  Dirección
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {compania.address || '-'}
                </Card>
              </Grid>

            </Grid>
          </Card>

        </div>
    )
  }
}
export default withStyles(styles, {withTheme: true})(SimpleCardContent);