import React from 'react';
import {Icon, Table, TableHead, TableCell, TableRow, Typography, Paper, TableBody} from '@material-ui/core';
import clsx from 'clsx';
import Grid from "@material-ui/core/Grid";
import { Doughnut } from 'react-chartjs-2';
import { Bar } from 'react-chartjs-2';
import { Chart } from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';

function Widget10(tickets) {
  var theHelp = Chart.helpers;

  const dataServ = {
    edilicios: 0,
    administrativos: 0,
    sugerencias: 0
  };
  const dataEst = {
    pendiente: 0,
    enProgreso: 0,
    finalizado: 0,
    cancelado: 0
  };
  const count = [];
  const count2 = [];

  if (tickets.tickets) {
    tickets.tickets.map(filter => (
      filter.servicio.id === 1 ? (dataServ.edilicios = dataServ.edilicios + 1) : dataServ.edilicios,
      filter.servicio.id === 2 ? (dataServ.administrativos = dataServ.administrativos + 1) : dataServ.administrativos,
      filter.servicio.id === 3 ? (dataServ.sugerencias = dataServ.sugerencias + 1) : dataServ.sugerencias,
      filter.estadoId === 1 ? (dataEst.pendiente = dataEst.pendiente + 1) : dataEst.pendiente,
      filter.estadoId === 2 ? (dataEst.enProgreso = dataEst.enProgreso + 1) : dataEst.enProgreso,
      filter.estadoId === 3 ? (dataEst.finalizado = dataEst.finalizado + 1) : dataEst.finalizado,
      filter.estadoId === 4 ? (dataEst.cancelado = dataEst.cancelado + 1) : dataEst.cancelado
    ));
    count.push(dataServ.edilicios, dataServ.administrativos, dataServ.sugerencias);
    count2.push(dataEst.pendiente, dataEst.enProgreso, dataEst.finalizado, dataEst.cancelado);
  }

  const data = {
    labels: [
      'Edilicios',
      'Administrativos',
      'Sugerencias'
    ],
    datasets: [{
      data: count,
      backgroundColor: [
        '#B265F4',
        '#F53C56',
        '#52B6F5'
      ],
      hoverBackgroundColor: [
        'rgba(178,101,244,0.71)',
        'rgba(245,60,86,0.71)',
        'rgba(82,182,245,0.7)'
      ],
      borderWidth: 1
    }],
  };
  const legend = {
    "position": "right",
    labels: {
      generateLabels: function(chart) {
        var data = chart.data;
        if (data.labels.length && data.datasets.length) {
          return data.labels.map(function(label, i) {
            var meta = chart.getDatasetMeta(0);
            var ds = data.datasets[0];
            var arc = meta.data[i];
            var custom = arc && arc.custom || {};
            var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
            var arcOpts = chart.options.elements.arc;
            var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
            var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
            var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
            return {
              // And finally :
              text: label + ": " + ds.data[i],
              fillStyle: fill,
              strokeStyle: stroke,
              lineWidth: bw,
              hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
              index: i
            };
          });
        }
        return [];
      }
    }
  }
  const options = {
    animation: {
      animateRotate : true
    },
    title: {
      display: true,
      text: 'Servicios',
      fontSize: 16,
    }
  };

  const data2 = {
    labels: [
      'Pendiente',
      'En Progreso',
      'Finalizado',
      'Cancelado'
    ],
    datasets: [{
      data: count2,
      backgroundColor: [
        '#848484',
        '#FFE940',
        '#53E8A4',
        '#F53C56'
      ],
      hoverBackgroundColor: [
        'rgba(132,132,132,0.71)',
        'rgba(255,233,64,0.7)',
        'rgba(83,232,164,0.71)',
        'rgba(245,60,86,0.71)'
      ],
      borderWidth: 1
    }],
  };
  const legend2 = {
    "position": "right",
    display: true,
    labels: {
      generateLabels: function(chart) {
        var data = chart.data;
        if (data.labels.length && data.datasets.length) {
          return data.labels.map(function(label, i) {
            var meta = chart.getDatasetMeta(0);
            var ds = data.datasets[0];
            var arc = meta.data[i];
            var custom = arc && arc.custom || {};
            var getValueAtIndexOrDefault = theHelp.getValueAtIndexOrDefault;
            var arcOpts = chart.options.elements.arc;
            var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
            var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
            var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
            return {
              // And finally :
              text: label + ": " + ds.data[i],
              fillStyle: fill,
              strokeStyle: stroke,
              lineWidth: bw,
              hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
              index: i
            };
          });
        }
        return [];
      }
    }
  };
  const options2 = {
    animation: {
      animateRotate : true
    },
    title: {
      display: true,
      text: 'Estado de Solución',
      fontSize: 16,
    }
  };

  return (
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item xs={12} sm={6} style={{padding: '0px 16px'}}>
              <Paper className="w-full rounded-8 shadow-none border-1" style={{padding: '20px'}}>
                <Bar data={data2} options={options2} legend={legend2}/>
              </Paper>
            </Grid>
            <Grid item xs={12} sm={6} style={{padding: '0px 16px'}}>
              <Paper className="w-full rounded-8 shadow-none border-1" style={{padding: '20px'}}>
                <Doughnut data={data} options={options} legend={legend}/>
              </Paper>
            </Grid>
          </Grid>
    );
}

export default React.memo(Widget10);
