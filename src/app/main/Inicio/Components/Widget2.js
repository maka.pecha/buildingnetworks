import React from 'react';
import {Card, Icon, Typography} from '@material-ui/core';
import {useTheme} from '@material-ui/styles';
import Grid from "@material-ui/core/Grid";
import TrendingDownIcon from '@material-ui/icons/TrendingDown';


function Widget2(tickets)
{
    const theme = useTheme();

    let cancelados = 0;
    if (tickets.tickets) {
        tickets.tickets.map(filter => (
          filter.estadoId === 4 ? (cancelados = cancelados + 1) : cancelados
        ))
    }

    return (
        <Card className="w-full rounded-8 shadow-none border-1" style={{padding: 20, height: 100}}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
                <Grid item xs={9}>
                    <div style={{fontSize: 13, fontWeight: 'bold', color: '#8898AA'}}>
                        TICKETS CANCELADOS
                    </div>
                    <div style={{fontSize: 20, fontWeight: 'bold', color: '#4D4F5C'}}>
                        {cancelados}
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div style={{backgroundColor: '#00908F', color: 'white',
                        padding: 10, borderRadius: '50%', height: 50, width: 50,
                        display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <TrendingDownIcon/>
                    </div>
                </Grid>
            </Grid>
        </Card>
    );
}

export default React.memo(Widget2);
