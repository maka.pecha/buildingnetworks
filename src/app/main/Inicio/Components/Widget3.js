import React from 'react';
import {Card} from '@material-ui/core';
import {useTheme} from '@material-ui/styles';
import {Bar} from 'react-chartjs-2';
import {FuseAnimate} from '@fuse';
import CheckIcon from '@material-ui/icons/Check';
import Grid from "@material-ui/core/Grid";

function Widget3(tickets)
{
    const theme = useTheme();

    let finalizados = 0;
    if (tickets.tickets) {
        tickets.tickets.map(filter => (
          filter.estadoId === 3 ? (finalizados = finalizados + 1) : finalizados
        ))
    }

    return (
        <Card className="w-full rounded-8 shadow-none border-1" style={{padding: 20, height: 100}}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
                <Grid item xs={9}>
                    <div style={{fontSize: 13, fontWeight: 'bold', color: '#8898AA'}}>
                        TICKETS FINALIZADOS
                    </div>
                    <div style={{fontSize: 20, fontWeight: 'bold', color: '#4D4F5C'}}>
                        {finalizados}
                    </div>
                </Grid>
                <Grid item xs={3}>
                    <div style={{backgroundColor: '#00908F', color: 'white',
                        padding: 10, borderRadius: '50%', height: 50, width: 50,
                        display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <CheckIcon/>
                    </div>
                </Grid>
            </Grid>
        </Card>
    );
}

export default React.memo(Widget3);
