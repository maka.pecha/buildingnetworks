import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import Fab from "@material-ui/core/Fab";
import HomeIcon from '@material-ui/icons/Home';
import {Card, Typography} from '@material-ui/core';
import Widget1 from "./Components/Widget1";
import Widget2 from './Components/Widget2';
import Widget3 from "./Components/Widget3";
import Widget4 from "./Components/Widget4";
import Widget10 from "./Components/Widget10";
import {FuseAnimate} from '@fuse';
import {FuseAnimateGroup} from '@fuse';
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import moment from "moment";

const styles = theme => ({
    layoutRoot: {},
    title: {
      fontSize: 30,
      marginLeft: 30,
      marginTop: 20
    },
    widget: {
      borderRadius: 50
    },
    root: {
      flexWrap: "wrap",
      "& > *": {
        margin: theme.spacing(1)
      }
    },
});

class Inicio extends Component {

  constructor(props){
    super(props);
    this.state = {
      // TicketID : props.location.state ? props.location.state.TicketID : "",
      tickets : [],
      fecha: {},
      preTickets: [],
    };
  }

  componentDidMount(){
    this.FetchTickets();
  }

  changeFecha = event => {
    let fecha = {...this.state.fecha};
    fecha.id =  event.target.value;
    if (fecha.id === 1) { // ultimo dia
      fecha.desde = moment().format('YYYY-MM-DD');
      fecha.hasta = moment().format('YYYY-MM-DD');
    } else if (fecha.id === 2) { // ultimo dia
      fecha.desde = moment().subtract(1, 'weeks').format('YYYY-MM-DD');
      fecha.hasta = moment().format('YYYY-MM-DD');
    } else if (fecha.id === 3) { //ultimo mes
      fecha.desde = moment().subtract(1, 'months').format('YYYY-MM-DD');
      fecha.hasta = moment().format('YYYY-MM-DD');
    } else if (fecha.id === 4) { // historico
      fecha = null;
    }
    this.setState({fecha: fecha});
    setTimeout(() => {
      this.FilterTickets();
    }, 300)
  };

  FilterTickets() {
    let filter = this.state.preTickets;
    if (this.state.fecha && this.state.fecha.hasta) {
      filter = this.state.preTickets.filter( t =>
        {
          return new Date(this.state.fecha.desde).getTime() <= new Date(t.fechaAlta).getTime() &&
            new Date(this.state.fecha.hasta).getTime() >= new Date(t.fechaAlta).getTime();
        }
      );
    }
    this.setState({tickets: filter});
  }

  FetchTickets() {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    const url = `http://salsipuedes.capitalinasdc.com/api/admin/tickets?EntornoId=` +
      `${localStorage.getItem('id_entorno') || 1}`;
    fetch(url
      , requestOptions)
      .then(response => {
        return response.json()
      })
      .then(result => {
        this.setState({preTickets: result});
        setTimeout(() => {
          this.FilterTickets();
        }, 250);
      })
      .catch(error => console.log('error', error));
  }

  render()
    {
        const {classes} = this.props;
        return (
            <FusePageSimple
                classes={{
                    root: classes.layoutRoot
                }}
                header={
                    <Typography className={classes.title}>Inicio</Typography>
                }
                content={
                  <div>
                    <div style={{ display: 'flex', justifyContent: 'flex-end', position: 'absolute', right: 20, top: 25}}>
                      <Card className="rounded-8 shadow-none border-1" style={{ marginBlockEnd: 10, marginBlockStart:10,
                        marginRight: 30 }}>
                        <FormControl className={classes.formControl}>
                          <Select
                            defaultValue={4}
                            onChange={this.changeFecha}
                            style={{ padding: 10 }}
                          >
                            <MenuItem value={1}>Último día</MenuItem>
                            <MenuItem value={2}>Última semana</MenuItem>
                            <MenuItem value={3}>Último mes</MenuItem>
                            <MenuItem value={4}>Histórico</MenuItem>
                          </Select>
                        </FormControl>
                      </Card>
                    </div>
                    <div className="flex flex-col sm:flex sm:flex-row pb-32 pl-16 pr-16">
                        <FuseAnimate delay={400}>
                          <div className="widget flex w-full sm:w-1/4 p-12">
                            <Widget1 tickets={this.state.tickets} className={classes.widget} />
                          </div>
                        </FuseAnimate>
                        <FuseAnimate delay={400}>
                          <div className="widget flex w-full sm:w-1/4 p-12">
                            <Widget2 tickets={this.state.tickets} className={classes.widget} />
                          </div>
                        </FuseAnimate>
                        <FuseAnimate delay={400}>
                          <div className="widget flex w-full sm:w-1/4 p-12">
                            <Widget3 tickets={this.state.tickets} className={classes.widget} />
                          </div>
                        </FuseAnimate>
                        <FuseAnimate delay={400}>
                          <div className="widget flex w-full sm:w-1/4 p-12">
                            <Widget4 tickets={this.state.tickets} className={classes.widget} />
                          </div>
                        </FuseAnimate>
                    </div>
                    <FuseAnimateGroup
                      className="flex flex-wrap"
                      enter={{
                        animation: "transition.slideUpBigIn"
                      }}
                    >
                      <div className="widget flex w-full p-12">
                        <Widget10 tickets={this.state.tickets} />
                      </div>
                    </FuseAnimateGroup>

                  </div>
                }
            />
        )
    }
}

export default withStyles(styles, {withTheme: true})(Inicio);