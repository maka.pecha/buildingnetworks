import Inicio from './Inicio';

export const InicioConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/inicio',
            component: Inicio
        }
    ]
};

/**
 * Lazy load Inicio
 */
/*
import React from 'react';

export const InicioConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/inicio',
            component: React.lazy(() => import('./Inicio'))
        }
    ]
};
*/
