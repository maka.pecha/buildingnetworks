import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import {Link} from "react-router-dom";
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import Card from '@material-ui/core/Card';
import CardContent from "@material-ui/core/CardContent";
import Moment from "react-moment";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FiberManualRecordIcon from "@material-ui/core/SvgIcon/SvgIcon";

const styles = theme => ({
  root: {
    margin: 0,
    padding: 20,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: 'white',
  }
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

/**
 * @return {null}
 */

export default function CustomizedDialogs(ticket) {
  const [open, setOpen] = React.useState(false);
  const [scroll, setScroll] = React.useState('paper');

  const handleClickOpen = scrollType => () => {
    setOpen(true);
    setScroll(scrollType);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const descriptionElementRef = React.useRef(null);
  React.useEffect(() => {
    if (open) {
      const { current: descriptionElement } = descriptionElementRef;
      if (descriptionElement !== null) {
        descriptionElement.focus();
      }
    }
  }, [open]);

  return (
    ticket && ticket.ticket && ticket.messages ?
      <div>
        <Button style={{backgroundColor: '#00908F', color: 'white', padding: 10}} onClick={handleClickOpen('paper')}>
          <ChatBubbleOutlineIcon style={{paddingRight: 5}}/> Mensajes
        </Button>
        <Dialog scroll={scroll}
                maxWidth="md"
                fullWidth={true}
                onClose={handleClose}
                open={open}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title"
                       onClose={handleClose}
                       style={{color:'white', backgroundColor: '#122230', boxShadow: '0px 4px 3px -1px rgba(0,41,62,0.59)'}} >
            <div>
               <span>
                Ticket {ticket.ticket.codigo} - Usuario {ticket.userName}
              </span>
            </div>
          </DialogTitle>
            {ticket.messages && ticket.messages.length > 0 ?
          <DialogContent >
            <div
              id="scroll-dialog-description"
              ref={descriptionElementRef}
              tabIndex={-1}>
              {ticket.messages.map(filter => (
                <div
                style={{
                  width:'100%',
                  display: 'flex',
                  justifyContent: filter.usuarioId !== ticket.ticket.usuarioId ? 'flex-end' : 'flex-start'
                }}>
                  <Card
                    style={{
                      boxShadow: '2px -2px 5px 1px #00293e33',
                      borderRadius: 20,
                      padding: 20,
                      backgroundColor: filter.usuarioId !== ticket.ticket.usuarioId ? '#3583FF' : '#EDF0F5',
                      color: filter.usuarioId !== ticket.ticket.usuarioId ? 'white' : 'black',
                      marginBottom: 20,
                      minWidth: '30%',
                      maxWidth: '70%',
                      width: 'fit-content'
                    }}
                    value={filter.id} key={filter.id}
                  >
                    <CardContent style={{ padding: 'unset', fontSize: 12}} >
                      <Typography>{filter.texto}</Typography>
                        <Moment format="DD/MM/YYYY"
                                style={{
                                  color: filter.usuarioId !== ticket.ticket.usuarioId ? '#fafafac7' : 'grey',
                                  justifyContent: 'flex-end',
                                  display: 'flex',
                                  marginBottom: -15,
                                  fontSize: 11 }}>
                          {filter.fecha}
                        </Moment>
                    </CardContent>
                  </Card>
                </div>
              ))}
            </div>
          </DialogContent> :
          <DialogContent dividers={'paper'}>
            <DialogContentText id="scroll-dialog-description"
                               ref={descriptionElementRef}
                               tabIndex={-1}>
              <div className="p-24" style={{ width: '100%' }}>
                <Typography style={{color: 'grey', textAlign: 'center', fontSize: 25}}>
                  No se han encontrado Mensajes para este Ticket
                </Typography>
              </div>
            </DialogContentText>
          </DialogContent> }
            <DialogActions style={{backgroundColor: '#F1F3F9'}}>
              <TextField
                style={{ width: '85%', padding: 5, paddingRight: 20 }}
                label="Escribe tu mensaje aquí..."
                value={ticket.response.texto || ''}
                onChange={ticket.changeResponse}
                margin="normal"
              />
              <Link to='tickets-content'>
                <Button style={{backgroundColor: '#00908F', color: 'white', padding: 10}}
                        autoFocus onClick={ticket.saveMessage} color="primary">
                  Enviar
                </Button>
              </Link>
            </DialogActions>
        </Dialog>
      </div>
      :
      null
  );
}