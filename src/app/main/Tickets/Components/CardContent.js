import React, {Component} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Moment from "react-moment";
import Grid from "@material-ui/core/Grid";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import {Link} from "react-router-dom";
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import ModalGuardar from "../Components/ModalGuardar";
import Chat from "../Chat";

const styles = theme => ({
  layoutRoot: {},
  card: {
    width: '100%',
    minWidth: 100,
    backgroundColor: '#F1F3F9',
    marginBottom: 25,
    fontSize: 20,
    padding: 10,
  },
  cardHeader: {
    padding: '30px',
    borderRadius: 20,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 6,
    fontSize: 18,
  },
  textField: {
    width: 200,
  },
  margin: {
    margin: theme.spacing(1),
  },
  header: {
    color: '#172a3a',
    fontSize: 22,
  },
  formControl: {
    minWidth: 120,
    backgroundColor: '#f1f3f9',
    width: '100%',
    padding: 10,
  },
});

class SimpleCardContent extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
  }

  render () {
    const {classes, ticket, changeMotivo, changeEstado, changeResponse, saveTicket, saveMessage, messages, response, userName} = this.props;
    let motivos = [];

    if (ticket.id && ticket.servicio.id == 1){
      motivos = [{id: 1, name: 'Electricidad'}, {id: 2, name: 'Plomería'}, {id: 3, name: 'Aberturas'}, {id: 4, name: 'Ascensores'}, {id: 5, name: 'Espacios Comunes'}, {id: 6, name: 'Otros'}];
    } else if (ticket.id && ticket.servicio.id == 2) {
      motivos = [{id: 7, name: 'Expensas'}, {id: 8, name: 'Solicitud de Tarjetas'}, {id: 9, name: 'Otros'}];
    } else if (ticket.id && ticket.servicio.id == 3) {
      motivos = [{id: 1, name: 'Sugerencias'}];
    }

    return(
      (ticket.id ?
        <div>
          <Card className={classes.cardHeader}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Typography className={classes.header} style={{paddingBlockEnd: 25}}>
                <Link to={{ pathname: '/tickets'}}>
                  <ArrowBackIosIcon style={{color:'#192d3e'}} />
                </Link>
                Detalles del Ticket
              </Typography>
              <Chat ticket={ticket} messages={messages} userName={userName} response={response} changeResponse={changeResponse} saveMessage={saveMessage}/>
            </Grid>

            <Grid container spacing={5}>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Servicio
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {ticket.servicio.nombre}
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Motivo
                </Typography>
                <Card className={classes.card} color="textSecondary" style={{padding: 0}}>
                  <FormControl className={classes.formControl}>
                    <Select
                      value={ticket.motivoId}
                      onChange={(e) => changeMotivo(e, 'motivoId')}
                    >
                      {motivos.map(filter => (
                        <MenuItem value={filter.id} key={filter.id} >{filter.name}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Descripción
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {ticket.descripcionServicio || '-'}
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Estado
                </Typography>
                <Card className={classes.card} color="textSecondary" style={{padding: 0}}>
                  <FormControl className={classes.formControl}>
                    <Select
                      value={ticket.estadoId}
                      onChange={changeEstado}
                    >
                      <MenuItem value={1}>Pendiente</MenuItem>
                      <MenuItem value={2}>En Progreso</MenuItem>
                      <MenuItem value={3}>Finalizado</MenuItem>
                      <MenuItem value={4}>Cancelado</MenuItem>
                    </Select>
                  </FormControl>
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Fecha
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  <Moment format=" DD/MM/YYYY">
                    {ticket.fechaAlta}
                  </Moment>
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Observaciones del estado:
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {ticket.estadoObservacion || '-'}
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Torre
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {ticket.torre.nombre}
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Sector
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {ticket.sector}
                </Card>
              </Grid>
              <Grid item xs={9} sm={4}>
                <Typography className={classes.header}>
                  Observaciones de la ubicación:
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  {ticket.descripcionSector || '-'}
                </Card>
              </Grid>
              {ticket.imagen ?
              <Grid item xs={12} sm={3} style={{marginBottom: 30}}>
                <Typography className={classes.header} style={{ paddingBottom: 5 }}>
                  Imagen
                </Typography>
                <img
                  style={{borderRadius: 20, boxShadow: '4px 4px 10px 0px rgba(0,0,0,0.5)'}}
                  src={`http://salsipuedes.capitalinasdc.com/api/tickets/imagenes/${ticket.imagen}`}
                  alt="imageTicket"
                />
              </Grid> : null}
            </Grid>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
            >
              <ModalGuardar saveTicket={saveTicket}/>
            </Grid>
          </Card>

        </div>
      : false)
    )
  }
}
export default withStyles(styles, {withTheme: true})(SimpleCardContent);