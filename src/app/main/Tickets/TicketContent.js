import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import SimpleCardContent from './Components/CardContent';
import {Typography} from "@material-ui/core";
import moment from 'moment';

const styles = theme => ({
  layoutRoot: {},
  title: {
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20
  },
  back: {
    marginBottom: 6,
    fontSize: 18,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  pos: {
    fontSize: 18,
    margin: theme.spacing(1.2),
    color: '#a7dae6',
  },
});

class TicketContent extends Component {
  constructor(props){
    super(props);
    this.state = {
      TicketID : props.location.state ? props.location.state.TicketID : "",
      ticket : {},
      messages: {},
      user: {},
      response: {},
      currentUser: {}
    };
  }

  componentDidMount(){
    this.FetchTicket();
    this.FetchTicketMessages();
    this.FetchCurrentUser();
  }

  changeMotivo = (event, property) => {
    const ticketCopy = {...this.state.ticket};          // Creo la copia de Tickets para cambiar solo una propiedad del state Tickets
    ticketCopy[property] =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
    this.setState({ticket: ticketCopy});          // Escribo en el state el nuevo cambio
  };

  changeEstado = event => {
    const ticketCopy = {...this.state.ticket};          // Creo la copia de Tickets para cambiar solo una propiedad del state Tickets
    ticketCopy.estadoId =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
    this.setState({ticket: ticketCopy});          // Escribo en el state el nuevo cambio
  };

  changeResponse = event => {
    const responseCopy = {...this.state.response};          // Creo la copia de Tickets para cambiar solo una propiedad del state Tickets
    responseCopy.texto =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
    responseCopy.fecha = moment();
    responseCopy.ticketId = this.state.TicketID;
    responseCopy.usuarioId = this.state.currentUser.id;
    this.setState({response: responseCopy});          // Escribo en el state el nuevo cambio
  };

  saveTicket = event => {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(this.state.ticket);

    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/admin/tickets", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  };

  saveMessage = event => {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(this.state.response);

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/tickets/" + this.state.TicketID + "/mensajes", requestOptions)
      .then(response => response.text())
      .then(result => {
        this.FetchTicketMessages();
      })
      .catch(error => console.log('error', error));
  };

  FetchTicket(){
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/tickets/" + this.state.TicketID, requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({ticket : result});
        this.FetchUsuario();
      })
      .catch(error => console.log('error', error));
  }

  FetchTicketMessages(){
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/tickets/" + this.state.TicketID + "/mensajes", requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({messages : result});
        const event = {};
        event['target'] = {};
        event['target']['value'] = null;
        this.changeResponse(event);
      })
      .catch(error => console.log('error', error));
  }

  FetchCurrentUser() {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/usuarios?minimalUser=true", requestOptions)
      .then(response => response.json())
      .then(result =>  {
        this.setState({currentUser : result});
      })
      .catch(error => console.log('error', error));
  }

  FetchUsuario() {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios/" + this.state.ticket.usuarioId, requestOptions)
      .then(response => response.json())
      .then(result =>  {
        this.setState({user : result});
      })
      .catch(error => console.log('error', error));
  }

  render()
  {
    const {classes} = this.props;
    return (
      <FusePageSimple
        classes={{
          root: classes.layoutRoot
        }}
        header={
          <Typography className={classes.title}>
           Ticket - {this.state.ticket.codigo}
          </Typography>
        }
        content={
          <div className="p-24">
            <br/>
              <SimpleCardContent
                ticket={this.state.ticket}
                userName={this.state.user.userName}
                messages={this.state.messages}
                response={this.state.response}
                changeMotivo={this.changeMotivo}
                changeEstado={this.changeEstado}
                saveTicket={this.saveTicket}
                saveMessage={this.saveMessage}
                changeResponse={this.changeResponse}
              />
            <br/>
          </div>
        }
      />
    )
  }

}

export default withStyles(styles, {withTheme: true})(TicketContent);