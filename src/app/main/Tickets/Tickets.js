import React, {Component} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import {Link} from "react-router-dom";
import {Typography} from "@material-ui/core";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Moment from "react-moment";
import Card from '@material-ui/core/Card';
import clsx from "clsx";
import {TextField} from '@material-ui/core';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import moment from "moment";

const styles = theme => ({
    layoutRoot: {},
    title: {
      fontSize: 30,
      marginLeft: 30,
      marginTop: 20,
    },
    card: {
      backgroundColor: '#FFE940',
      textAlign: 'center',
      color: 'black',
      borderRadius: 8,
      boxShadow: 'none',
      padding: '2px 10px',
    },
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    Cancelado: {
      backgroundColor: '#F53C56',
      color: 'white',
    },
    Pendiente: {
      backgroundColor: '#848484',
      color: 'white',
    },
    'En Progreso': {
      backgroundColor: 'yellow',
    },
    Finalizado: {
      backgroundColor: '#53E8A4',
      color: 'white',
    },
    Edilicios: {
      color: '#B265F4',
    },
    Administrativos: {
      color: '#F53C56',
    },
    Sugerencias: {
      color: '#52B6F5',
    },
    Higiene: {
      color: '#FEB969',
    },
});

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

const initialTicket = {
  Fecha: null,
  FechaToSend: null,
  Estado: null,
  Ubicacion: null,
  NroTicket: null,
  ServicioId: null,
  MotivoId: null,
}

class Tickets extends Component {
    constructor(props){
      super(props);
      this.state = {
        ticketsList: null,
        motivosList: null,
        serviciosList: null,
        estadosList: null,
        torresList: null,
        ticket: {
          Fecha: null,
          FechaToSend: null,
          Estado: '',
          Ubicacion: '',
          NroTicket: '',
          ServicioId: '',
          MotivoId: '',
        },
        usuariosList: [],
      };
      this.componentDidMount();
    }

    componentDidMount() {
      this.FetchUsuarios();
      this.FetchMotivos();
      this.FetchServicios();
      this.FetchEstados();
      this.FetchTorres();
    }

    HandleChange = (event, property) => {
      const ticketCopy = {
          Fecha: null,
          FechaToSend: null,
          Estado: '',
          Ubicacion: '',
          NroTicket: '',
          ServicioId: '',
          MotivoId: '',
        };          // Creo la copia de user para cambiar solo una propiedad del state usuarios
      ticketCopy[property] =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
      this.setState({ticket: ticketCopy});          // Escribo en el state el nuevo cambio
      setTimeout(() => {
      this.FetchTicket();
      }, 500)
    };

    HandleDateChange = (event, property) => {
      const ticketCopy = {
        Fecha: null,
        FechaToSend: null,
        Estado: '',
        Ubicacion: '',
        NroTicket: '',
        ServicioId: '',
        MotivoId: '',
      };           // Creo la copia de user para cambiar solo una propiedad del state usuarios
      ticketCopy[property] = event;                    // Me trae el valor directamente por eso pongo solo event
      ticketCopy[property + 'ToSend'] =  moment(event).format('YYYY-MM-DD');                    // Me trae el valor directamente por eso pongo solo event
      this.setState({ticket: ticketCopy});          // Escribo en el state el nuevo cambio
      setTimeout(() => {
        this.FetchTicket()
      }, 500)
    };

    FetchMotivos() {
      const token = `Bearer ${localStorage.getItem('id_token')}`;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", token);

      const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch("http://salsipuedes.capitalinasdc.com/api/motivos/", requestOptions)
        .then(response => response.json())
        .then(result => {
          this.setState({motivosList : result});
        })
        .catch(error => console.log('error', error));
    }

    FetchServicios() {
      const token = `Bearer ${localStorage.getItem('id_token')}`;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", token);

      const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch(`http://salsipuedes.capitalinasdc.com/api/entornos/${localStorage.getItem('id_entorno') || 1}/servicios/`, requestOptions)
        .then(response => response.json())
        .then(result => {
          this.setState({serviciosList : result});
        })
        .catch(error => console.log('error', error));
    }

    FetchEstados() {
      const token = `Bearer ${localStorage.getItem('id_token')}`;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", token);

      const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch("http://salsipuedes.capitalinasdc.com/api/estados/", requestOptions)
        .then(response => response.json())
        .then(result => {
          this.setState({estadosList : result});
        })
        .catch(error => console.log('error', error));
    }

    FetchTorres() {
      const token = `Bearer ${localStorage.getItem('id_token')}`;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", token);

      const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch(`http://salsipuedes.capitalinasdc.com/api/entornos/${localStorage.getItem('id_entorno') || 1}/torres/`, requestOptions)
        .then(response => response.json())
        .then(result => {
          this.setState({torresList : result});
        })
        .catch(error => console.log('error', error));
    }

    FetchTicket() {
      const token = `Bearer ${localStorage.getItem('id_token')}`;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", token);

      const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      const url = `http://salsipuedes.capitalinasdc.com/api/admin/tickets?EntornoId=` +
        `${localStorage.getItem('id_entorno') || 1}` +
        `${this.state.ticket.FechaToSend ? '&Fecha=' + this.state.ticket.FechaToSend : ''}` +
        `${this.state.ticket.Estado ? '&Estado=' + this.state.ticket.Estado : ''}` +
        `${this.state.ticket.Ubicacion ? '&Ubicacion=' + this.state.ticket.Ubicacion : ''}` +
        `${this.state.ticket.NroTicket ? '&NroTicket=' + this.state.ticket.NroTicket.toUpperCase() : ''}` +
        `${this.state.ticket.ServicioId ? '&ServicioId=' + this.state.ticket.ServicioId : ''}` +
        `${this.state.ticket.MotivoId ? '&MotivoId=' + this.state.ticket.MotivoId : ''}`;
      fetch(url
        , requestOptions)
        .then(response => {
          return response.json()
        })
        .then(result => {
          for ( let i in result) {
            const userID = result[i].usuarioId;
            const user = this.state.usuariosList.find(u => u.id === userID);
            result[i].user = user ? user.nombreCompleto : 'Usuario';
          }
          result.sort(function(a,b){
            return new Date(b.fechaAlta) - new Date(a.fechaAlta);
          });
          this.setState({ticketsList: result});
        })
        .catch(error => console.log('error', error));
    }

    FetchUsuarios() {
      const token = `Bearer ${localStorage.getItem('id_token')}`;
      const myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      myHeaders.append("Authorization", token);

      const requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow'
      };

      fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios", requestOptions)
        .then(response => response.json())
        .then(result => {
          this.setState({usuariosList: result});
        }).then(() => {
        this.FetchTicket();
      })
        .catch(error => console.log('error', error));
    }

    SimpleTable() {
    const { classes } = this.props;
    return (
      this.state.ticketsList && this.state.serviciosList && this.state.motivosList && this.state.estadosList && this.state.torresList ?
        <TableContainer component={Paper} style={{borderRadius: 20}}>
          <Table className={classes.table} aria-label="custom pagination table ">
            <TableHead>
              <TableRow>
                <TableCell>
                  <TextField
                    placeholder='Referencia'
                    value={this.state.ticket.NroTicket.toUpperCase() || ''}
                    style={{minWidth: 120}}
                    onChange={(e) => this.HandleChange(e, 'NroTicket')}
                    margin="normal"
                    required
                  /></TableCell>
                <TableCell align="left">Emisor</TableCell>
                <TableCell align="left">
                  <FormControl style={{minWidth: 100}}>
                    <InputLabel id="Servicio">Servicio</InputLabel>
                    <Select
                      labelId="Servicio"
                      margin="normal"
                      value={this.state.ticket.ServicioId || ''}
                      onChange={(e) => this.HandleChange(e, 'ServicioId')}
                    >
                      <MenuItem value="" selected>--</MenuItem>
                      {this.state.serviciosList.map(filter => (
                        <MenuItem value={filter.id} key={filter.id}>{filter.nombre}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </TableCell>
                <TableCell align="left">
                  <FormControl style={{minWidth: 100}}>
                    <InputLabel id="Estado">Estado</InputLabel>
                    <Select
                      labelId="Estado"
                      margin="normal"
                      value={this.state.ticket.Estado || ''}
                      onChange={(e) => this.HandleChange(e, 'Estado')}
                    >
                      <MenuItem value="" selected>--</MenuItem>
                      {this.state.estadosList.map(filter => (
                        <MenuItem value={filter.key} key={filter.key}>{filter.value}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </TableCell>
                <TableCell align="left">
                  <FormControl style={{minWidth: 100}}>
                    <InputLabel id="Motivo">Motivo</InputLabel>
                    <Select
                      labelId="Motivo"
                      margin="normal"
                      value={this.state.ticket.MotivoId || ''}
                      onChange={(e) => this.HandleChange(e, 'MotivoId')}
                    >
                      <MenuItem value="" selected>--</MenuItem>
                      {this.state.motivosList.map(filter => (
                        <MenuItem value={filter.id} key={filter.id}>{filter.id} - {filter.nombre}</MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </TableCell>
                <TableCell align="left">
                  <FormControl style={{minWidth: 100}}>
                    <InputLabel id="Torre">Torre</InputLabel>
                    <Select
                      labelId="Torre"
                      margin="normal"
                      value={this.state.ticket.Ubicacion || ''}
                      onChange={(e) => this.HandleChange(e, 'Ubicacion')}
                    >
                      <MenuItem value="" selected>--</MenuItem>
                      {this.state.torresList.map(filter => (
                        <MenuItem value={filter.id} key={filter.id}>{filter.nombre}</MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </TableCell>
                <TableCell align="left">Sector</TableCell>
                <TableCell align="right">
                  <FormControl style={{minWidth: 100}}>
                    {!this.state.ticket.Fecha ? <InputLabel id="Torre">Fecha</InputLabel> : null}
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        format="dd/MM/yyyy"
                        margin="normal"
                        disableFuture={true}
                        maxDateMessage="No se pueden ingresar fechas futuras"
                        invalidDateMessage="Formato de fecha inválido"
                        id="date-picker-inline"
                        value={this.state.ticket && this.state.ticket.Fecha ? this.state.ticket.Fecha : null || null}
                        onChange={(e) => this.HandleDateChange(e, 'Fecha')}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormControl>
                </TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            {this.state.ticketsList.length > 0 ?
              <TableBody>
                {this.state.ticketsList.map(row => (
                  <StyledTableRow key={row.id}>
                    <StyledTableCell>{row.codigo}</StyledTableCell>
                    <StyledTableCell>{row.user}</StyledTableCell>
                    <StyledTableCell padding="checkbox" component="th" scope="row">
                      <FiberManualRecordIcon className={classes[row.servicio.nombre]} fontSize={'small'}/>
                      {row.servicio.nombre}
                    </StyledTableCell>
                    <StyledTableCell align="left">
                      <Card className={clsx(classes.card, classes[row.estado])}>
                        <Typography>
                          {row.estado}
                        </Typography>
                      </Card>
                    </StyledTableCell>
                    <StyledTableCell align="left">{row.motivo.nombre}</StyledTableCell>
                    <StyledTableCell align="left">{row.torre.nombre}</StyledTableCell>
                    <StyledTableCell align="left">{row.sector}</StyledTableCell>
                    <StyledTableCell padding="checkbox" align="left" fontSize={'small'}>
                      <Moment format=" DD/MM/YYYY">
                        {row.fechaAlta}
                      </Moment>
                    </StyledTableCell>
                    <StyledTableCell align="right" >
                      <Link to={{ pathname: '/tickets-content', state: { TicketID: row.id} }} >
                        <MoreVertIcon fontSize={'large'} style={{color:'#192d3e'}} />
                      </Link>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody> :
              <TableBody>
                <StyledTableRow>
                  <StyledTableCell>
                    <div className="p-24" style={{ position: 'absolute', width: '100%' }}>
                      <br/>
                      <Typography style={{color: 'grey', textAlign: 'center', marginTop: 50, fontSize: 25}}>
                        No se han encontrado Tickets con este filtro
                      </Typography>
                      <br/>
                    </div>
                  </StyledTableCell>
                </StyledTableRow>
              </TableBody>
              }
          </Table>
        </TableContainer>
        : null
    );
  }

    render()
    {
        const {classes} = this.props;
        return (
          this.state.ticketsList ?
            <FusePageSimple
                classes={{
                    root: classes.layoutRoot
                }}
                header={
                    <Typography className={classes.title}>
                      Tickets
                    </Typography>
                }
                content={
                    <div className="p-24">
                        <br/>
                      {this.SimpleTable()}
                        <br/>
                    </div>
                }
            />
          : <FusePageSimple
              classes={{
                root: classes.layoutRoot
              }}
              header={
                <Typography className={classes.title}>
                  Tickets
                </Typography>
              }
              content={
                <div className="p-24">
                  <br/>
                  <Typography style={{color: 'grey', textAlign: 'center', marginTop: 50, fontSize: 25}}>
                      No se han encontrado Tickets en este entorno
                  </Typography>
                  <br/>
                </div>
              }
            />

        )
    }
}

export default withStyles(styles, {withTheme: true})(Tickets);