import Tickets from './Tickets';
import Chat from './Chat';
import TicketContent from "./TicketContent";

export const TicketsConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/tickets',
            component: Tickets
        },
        {
            path     : '/tickets-content',
            component: TicketContent
        },
        {
            path     : '/chat',
            component: Chat
        }
    ]
};

/**
 * Lazy load Tickets
 */
