import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Link} from "react-router-dom";
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';

export default function AlertDialog(deleteUser) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button style={{ color: 'red', padding: 10, border: '1px solid'}} onClick={handleClickOpen}>
        Eliminar Usuario
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle style={{paddingTop: 50, textAlign: 'center'}} id="customized-dialog-title">
          Está seguro que desea eliminar el usuario?
          <DeleteOutlineIcon fontSize='large' style={{backgroundColor: '#f44336', color: 'white',
            padding: 10, borderRadius: '50%', height: 50, width: 50,
            justifyContent: 'center', alignItems: 'center', marginLeft: 20}}/>
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancelar
          </Button>
          <Link to='/usuarios' onClick={ ()=> deleteUser.deleteUser() }>
            <Button onClick={handleClose} color="primary" autoFocus>
              Aceptar
            </Button>
          </Link>
        </DialogActions>
      </Dialog>
    </div>
  );
}