import React, {Component} from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import TextField from "@material-ui/core/TextField";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import moment from 'moment';
import ModalGuardar from "./ModalGuardar";
import CheckIcon from "@material-ui/core/SvgIcon/SvgIcon";

const styles = theme => ({
  layoutRoot: {},
  card: {
    width: '100%',
    minWidth: 100,
    backgroundColor: '#F1F3F9',
    marginBottom: 25,
    fontSize: 20,
    paddingLeft: 10,
    paddingRight: 10
  },
  cardHeader: {
    padding: '30px',
    borderRadius: 20,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  pos: {
    marginBottom: 6,
    fontSize: 18,
  },
  textField: {
    width: 200,
  },
  margin: {
    margin: theme.spacing(1),
  },
  header: {
    color: '#172a3a',
    fontSize: 22,
  },
  formControl: {
    minWidth: 120,
    backgroundColor: '#f1f3f9',
    width: '100%',
    padding: 10,
  },
});

class SimpleCardContent extends Component {
  constructor(props){
    super(props);
    this.state = {
    };
  }

  render () {
    const {classes, user, saveUser, handleChange, handleDateChange} = this.props;

    return(
        <div>
          <Card className={classes.cardHeader}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Typography className={classes.header} style={{paddingBlockEnd: 25}}>
                <Link to={{ pathname: '/usuarios'}}>
                  <ArrowBackIosIcon style={{color:'#192d3e'}} />
                </Link>
                Datos de usuario
              </Typography>
            </Grid>
            <Grid container spacing={5}>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  <AcUnitIcon style={{fontSize:'x-small', color: '#f44336', marginBottom: 15}}/>
                  Nombre de usuario
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  <TextField
                    style={{width: '100%'}}
                    value={user.userName}
                    onChange={(e) => handleChange(e, 'userName')}
                    margin="normal"
                    required
                  />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  <AcUnitIcon style={{fontSize:'x-small', color: '#f44336', marginBottom: 15}}/>
                  Contraseña
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.password}
                                                                                onChange={(e) => handleChange(e, 'password')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  <AcUnitIcon style={{fontSize:'x-small', color: '#f44336', marginBottom: 15}}/>
                  Direccion de Email
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.email}
                                                                                onChange={(e) => handleChange(e, 'email')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  <AcUnitIcon style={{fontSize:'x-small', color: '#f44336', marginBottom: 15}}/>
                  Nombre completo
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.nombreCompleto}
                                                                                onChange={(e) => handleChange(e, 'nombreCompleto')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  <AcUnitIcon style={{fontSize:'x-small', color: '#f44336', marginBottom: 15}}/>
                  Sexo
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  <FormControl className={classes.formControl}>
                    <Select
                      value={user.sexo}
                      onChange={(e) => handleChange(e, 'sexo')}
                      required
                    >
                      <MenuItem value={0}>Femenino</MenuItem>
                      <MenuItem value={1}>Masculino</MenuItem>
                    </Select>
                  </FormControl>
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  <AcUnitIcon style={{fontSize:'x-small', color: '#f44336', marginBottom: 15}}/>
                  Rol
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  <FormControl className={classes.formControl}>
                    <Select
                      value={user.rolId}
                      onChange={(e) => handleChange(e, 'rolId')}
                      required
                    >
                      <MenuItem value={1}>Administrador</MenuItem>
                      <MenuItem value={2}>Usuario</MenuItem>
                    </Select>
                  </FormControl>
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  DNI
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.dni}
                                                                                onChange={(e) => handleChange(e, 'dni')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Puesto
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.puesto}
                                                                                onChange={(e) => handleChange(e, 'puesto')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Celular
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.telCelular}
                                                                                onChange={(e) => handleChange(e, 'telCelular')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Teléfono
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.telFijo}
                                                                                onChange={(e) => handleChange(e, 'telFijo')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Fecha de Nacimiento
                </Typography>
                <Card className={classes.card} color="textSecondary">
                  <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                      format="dd/MM/yyyy"
                      margin="normal"
                      id="date-picker-inline"
                      value={user.fechaNacimiento}
                      maxDateMessage="No se pueden ingresar fechas futuras"
                      invalidDateMessage="Formato de fecha inválido"
                      disableFuture={true}
                      onChange={(e) => handleDateChange(e, 'fechaNacimiento')}
                      KeyboardButtonProps={{
                        'aria-label': 'change date',
                      }}
                    />
                  </MuiPickersUtilsProvider>
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Dirección
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.domicilio}
                                                                                onChange={(e) => handleChange(e, 'domicilio')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Ciudad
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.ciudad}
                                                                                onChange={(e) => handleChange(e, 'ciudad')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
              <Grid item xs={12} sm={3}>
                <Typography className={classes.header}>
                  Provincia
                </Typography>
                <Card className={classes.card} color="textSecondary"><TextField style={{width: '100%'}}
                                                                                value={user.provincia}
                                                                                onChange={(e) => handleChange(e, 'provincia')}
                                                                                margin="normal"
                />
                </Card>
              </Grid>
            </Grid>
            <Grid
              container
              direction="row"
              justify="flex-end"
              alignItems="center"
            >
              {(user.userName && user.password && user.email && user.nombreCompleto && user.sexo !== null && user.rolId) ? <ModalGuardar
                saveUser={saveUser}
              /> : null}

            </Grid>
          </Card>
        </div>
    )
  }
}
export default withStyles(styles, {withTheme: true})(SimpleCardContent);
