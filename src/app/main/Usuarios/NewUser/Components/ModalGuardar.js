import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import CheckIcon from '@material-ui/icons/Check';
import {Link} from "react-router-dom";

const styles = theme => ({
  root: {
    margin: 0,
    padding: 20
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs(saveUser) {
  const [open, setOpen] = React.useState(false);
  const handleClickOpen = () => {
    setOpen(true);
    saveUser.saveUser();
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button style={{backgroundColor: '#00908F', color: 'white', padding: 10}} onClick={handleClickOpen}>
        Guardar Cambios
      </Button>
      <Dialog maxWidth="xs" fullWidth={true} onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
        <DialogTitle style={{paddingTop: 50, textAlign: 'center'}} id="customized-dialog-title">
          Datos Guardados con éxito
          <CheckIcon fontSize='large' style={{backgroundColor: '#4caf50', color: 'white',
            padding: 10, borderRadius: '50%', height: 50, width: 50,
            justifyContent: 'center', alignItems: 'center', marginLeft: 20}}/>
        </DialogTitle>
        <DialogActions>
          <Link to='/usuarios'>
            <Button style={{backgroundColor: '#00908F', color: 'white', padding: 10}}
                    autoFocus onClick={handleClose} color="primary">
              Aceptar
            </Button>
          </Link>
        </DialogActions>
      </Dialog>
    </div>
  );
}