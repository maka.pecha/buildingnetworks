import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import SimpleCardContent from './Components/CardContent';
import {Typography} from "@material-ui/core";
import moment from 'moment';

const styles = theme => ({
  layoutRoot: {},
  title: {
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20,
  },
  back: {
    marginBottom: 6,
    fontSize: 18,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  pos: {
    fontSize: 18,
    margin: theme.spacing(1.2),
    color: '#a7dae6',
  },
});

class NewUser extends Component {
  constructor(props){
    super(props);
    this.state = {
      UserID : props.location.state ? props.location.state.UserID : "",
      User : {},
    };
  }

  handleChange = (event, property) => {
    const userCopy = {...this.state.User};          // Creo la copia de user para cambiar solo una propiedad del state usuarios
    userCopy[property] =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
    if (userCopy.sexo == 0){
      userCopy.sexoDescripcion = 'Femenino';
    } else if (userCopy.sexo == 1) {
      userCopy.sexoDescripcion = 'Masculino';
    } else if (!userCopy.fechaNacimiento) {
      userCopy.fechaNacimiento = moment();
    }
    this.setState({User: userCopy});          // Escribo en el state el nuevo cambio
  };

  handleDateChange = (event, property) => {
    const userCopy = {...this.state.User};          // Creo la copia de user para cambiar solo una propiedad del state usuarios
    userCopy[property] =  event;                    // Me trae el valor directamente por eso pongo solo event
    this.setState({User: userCopy});
  };

  saveUser = event => {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(this.state.User);

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios", requestOptions)
      .then(response => response.json())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  render()
  {
    const {classes} = this.props;
    return (
      <FusePageSimple
        classes={{
          root: classes.layoutRoot
        }}
        header={
          <Typography className={classes.title}>
            Nuevo Usuario
          </Typography>
        }
        content={
          <div className="p-24">
            <br/>
              <SimpleCardContent
                user={this.state.User}
                saveUser={this.saveUser}
                handleChange={this.handleChange}
                handleDateChange={this.handleDateChange}
              />
            <br/>
          </div>
        }
      />
    )
  }

}

export default withStyles(styles, {withTheme: true})(NewUser);