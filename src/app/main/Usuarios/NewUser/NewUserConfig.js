import NewUser from './NewUser';

export const NewUserConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
    {
        path     : '/newUser',
        component: NewUser
    }
    ]
};

/**
 * Lazy load Usuarios
 */
/*
import React from 'react';

export const UsuariosConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/usuarios',
            component: React.lazy(() => import('./Usuarios'))
        }
    ]
};
*/
