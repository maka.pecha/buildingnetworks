import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import SimpleCardContent from './Components/CardContent';
import {Typography} from "@material-ui/core";
import {Redirect} from "react-router";

const styles = theme => ({
  layoutRoot: {},
  title: {
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20,
  },
  back: {
    marginBottom: 6,
    fontSize: 18,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  pos: {
    fontSize: 18,
    margin: theme.spacing(1.2),
    color: '#a7dae6',
  },
});

class UserContent extends Component {
  constructor(props){
    super(props);
    this.state = {
      UserID : props.location.state ? props.location.state.UserID : "",
      User : {},
    };
  }

  componentDidMount(){
    this.FetchUser();
  }

  handleChange = (event, property) => {
    const userCopy = {...this.state.User};          // Creo la copia de user para cambiar solo una propiedad del state usuarios
    userCopy[property] =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
    if (userCopy.sexo == 0){
      userCopy.sexoDescripcion = 'Femenino';
    } else if (userCopy.sexo == 1) {
      userCopy.sexoDescripcion = 'Masculino';
    }
    this.setState({User: userCopy});          // Escribo en el state el nuevo cambio
  };

  handleDateChange = (event, property) => {
    const userCopy = {...this.state.User};          // Creo la copia de user para cambiar solo una propiedad del state usuarios
    userCopy[property] =  event;                    // Me trae el valor directamente por eso pongo solo event
    this.setState({User: userCopy});
  };

  saveUser = event => {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(this.state.User);

    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  deleteUser = event =>{
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'DELETE',
      headers: myHeaders,
      redirect: 'follow'
    };

    const userID = this.state.UserID;

    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios/" + userID, requestOptions)
      .then(response => response.text())
      .then(result => {
        this.setState({User : result});
      })
      .catch(error => console.log('error', error));
  }

  FetchUser(){
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios/" + this.state.UserID, requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({User : result});
      })
      .catch(error => console.log('error', error));
  }

  render()
  {
    const {classes} = this.props;
    return (
      <FusePageSimple
        classes={{
          root: classes.layoutRoot
        }}
        header={
          <Typography className={classes.title}>
            Usuario - {this.state.User.nombreCompleto}
          </Typography>
        }
        content={
          <div className="p-24">
            <br/>
              <SimpleCardContent
                user={this.state.User}
                saveUser={this.saveUser}
                handleChange={this.handleChange}
                handleDateChange={this.handleDateChange}
                deleteUser={this.deleteUser}
              />
            <br/>
          </div>
        }
      />
    )
  }

}

export default withStyles(styles, {withTheme: true})(UserContent);