import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {FusePageSimple, DemoContent} from '@fuse';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Fab from '@material-ui/core/Fab';
import {Typography} from "@material-ui/core";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {Link} from "react-router-dom";
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Grid from "@material-ui/core/Grid";
import ArrowBackIosIcon from "@material-ui/core/SvgIcon/SvgIcon";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import AddIcon from '@material-ui/icons/Add';
import MaterialTable from 'material-table';
import UserContent from "./UserContent";
import history from '@history';

const styles = theme => ({
    layoutRoot: {},
    title: {
      fontSize: 30,
      marginLeft: 30,
      marginTop: 20
    },
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    table: {
      minWidth: 700,
    },
});

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
}))(TableRow);

class Usuarios extends Component {

  constructor(props) {
    super(props);
    this.state = {
      usuariosList: null,
      flag: false,
    };
    this.componentDidMount();
  }

  componentDidMount() {
    this.FetchUsuarios()
  }

  FetchUsuarios() {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };

    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios", requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({usuariosList: result});
      })
      .catch(error => console.log('error', error));
  }

  handleChange (event, id) {
    history.push({
      pathname: '/usuarios-content',
      state:  {UserID: id}
    });
  };

  render()
    {
        const {classes} = this.props;
        return (
            <FusePageSimple
                classes={{
                    root: classes.layoutRoot
                }}
                header={
                    <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="center"
                    >
                      <Typography className={classes.title}>
                        Usuarios
                      </Typography>
                      <Link to={{ pathname: '/newUser'}}>
                        <Button style={{backgroundColor: '#01908E', color: 'white', padding: 10, marginRight: 30}}>
                          <AddIcon style={{paddingRight: 5}}/> Nuevo
                        </Button>
                      </Link>

                    </Grid>
                }
                content={
                    <div className="p-24">
                      <br/>
                      { this.state.usuariosList ?
                        <MaterialTable
                          style={{ borderRadius: 20 }}
                          title=""
                          columns={[
                            { title: 'ID', field: 'id', type: 'numeric' },
                            { title: 'Nombre', field: 'nombreCompleto' },
                            { title: 'Puesto', field: 'puesto'},
                            { title: 'Email', field: 'email',},
                            ]}
                          data={this.state.usuariosList}
                          localization={{
                              header: {
                                actions:'Acción'
                              },
                              toolbar: {
                                searchTooltip: 'Buscar',
                                searchPlaceholder: 'Buscar'
                              },
                              pagination: {
                                labelRowsSelect: 'Filas',
                                labelRowsPerPage: 'Filas por página:',
                                firstAriaLabel: 'Primera página',
                                firstTooltip: 'Primera página',
                                previousAriaLabel: 'Anterior página',
                                previousTooltip: 'Anterior página',
                                nextAriaLabel: 'Siguiente página',
                                nextTooltip: 'Siguiente página',
                                lastAriaLabel: 'Ultima página',
                                lastTooltip: 'Ultima página'
                              }
                          }}
                          actions={[
                            {
                              icon: () => <MoreVertIcon/>,
                              tooltip: 'Editar',
                              onClick: (event, rowData) => this.handleChange(event, rowData.id)
                            }
                          ]}
                          components={{
                            Container: props => (
                              <TableContainer component={Paper} {...props} />
                            )
                          }}
                          options={{
                            actionsColumnIndex: -1
                          }}
                          /> : null }
                      <br/>
                    </div>
                }
            />
        )
    }
}

export default withStyles(styles, {withTheme: true})(Usuarios);