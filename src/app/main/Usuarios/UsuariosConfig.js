import Usuarios from './Usuarios';
import UserContent from "./UserContent";

export const UsuariosConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/usuarios',
            component: Usuarios
        },
        {
            path     : '/usuarios-content',
            component: UserContent
        }
    ]
};

/**
 * Lazy load Usuarios
 */
/*
import React from 'react';

export const UsuariosConfig = {
    settings: {
        layout: {
            config: {}
        }
    },
    routes  : [
        {
            path     : '/usuarios',
            component: React.lazy(() => import('./Usuarios'))
        }
    ]
};
*/
