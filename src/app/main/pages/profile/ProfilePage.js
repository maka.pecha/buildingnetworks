import React, {Component, useState} from 'react';
import {Avatar, Button, Tab, Tabs, Typography, withStyles} from '@material-ui/core';
import {makeStyles} from '@material-ui/styles';
import {FusePageSimple, FuseAnimate} from '@fuse';
import TimelineTab from './tabs/TimelineTab';
import PhotosVideosTab from './tabs/PhotosVideosTab';
import AboutTab from './tabs/AboutTab';
import * as userActions from 'app/auth/store/actions';
import SimpleCardContent from './Components/CardContent';

const styles = theme => ({
  layoutRoot: {},
  title: {
    fontSize: 30,
    marginLeft: 30,
    marginTop: 20
  },
  subtitle: {
    fontSize: 20,
    marginLeft: 30
  },
  back: {
    marginBottom: 6,
    fontSize: 18,
  },
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  pos: {
    fontSize: 18,
    margin: theme.spacing(1.2),
    color: '#a7dae6',
  },
});

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
    };
  }

  componentDidMount(){
    this.FetchUser();
  }

  handleChange = (event, property) => {
    const userCopy = {...this.state.user};          // Creo la copia de user para cambiar solo una propiedad del state usuarios
    userCopy[property] =  event.target.value;       // Cambio solo el id del servicio que me llego del Hijo
    if (userCopy.sexo == 0){
      userCopy.sexoDescripcion = 'Femenino';
    } else {
      userCopy.sexoDescripcion = 'Masculino';
    }
    this.setState({user: userCopy});          // Escribo en el state el nuevo cambio
  };

  handleDateChange = (event, property) => {
    const userCopy = {...this.state.user};          // Creo la copia de user para cambiar solo una propiedad del state usuarios
    userCopy[property] =  event;                    // Me trae el valor directamente por eso pongo solo event
    this.setState({user: userCopy});
  };

  saveUser = event => {
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Authorization", token);
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(this.state.user);

    const requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/admin/usuarios", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }

  FetchUser(){
    const token = `Bearer ${localStorage.getItem('id_token')}`;
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", token);

    const requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    fetch("http://salsipuedes.capitalinasdc.com/api/usuarios?minimalUser=false", requestOptions)
      .then(response => response.json())
      .then(result => {
        this.setState({user : result});
      })
      .catch(error => console.log('error', error));
  }

  render()
  {
    const {classes} = this.props;
    return (
      <FusePageSimple
        classes={{
          root: classes.layoutRoot
        }}
        header={
          <div>
            <Typography className={classes.title}>
              {this.state.user.userName}
            </Typography>
            <Typography className={classes.subtitle}>
              {this.state.user.puesto}
            </Typography>
          </div>
        }
        content={
          <div className="p-24">
            <br/>
            <SimpleCardContent
              user={this.state.user}
              saveUser={this.saveUser}
              handleChange={this.handleChange}
              handleDateChange={this.handleDateChange}

            />
            <br/>
          </div>
        }
      />
    )
  }

}

export default withStyles(styles, {withTheme: true})(ProfilePage);

// function ProfilePage()
// {
//
//     const classes = useStyles();
//     const [selectedTab, setSelectedTab] = useState(0);
//     const [user, setUser] = useState();
//
//     function handleTabChange(event, value)
//     {
//         setSelectedTab(value);
//     }
//
//     return (
//         call ? (
//             <FusePageSimple
//               classes={{
//                   header : classes.layoutHeader,
//                   toolbar: "px-16 sm:px-24"
//               }}
//               header={
//                   <div className="p-24 flex flex-1 flex-col items-center justify-center md:flex-row md:items-end">
//                       <div className="flex flex-1 flex-col items-center justify-center md:flex-row md:items-center md:justify-start">
//                           <FuseAnimate animation="transition.expandIn" delay={300}>
//                               <Avatar className="w-96 h-96" src="assets/images/avatars/profile.jpg"/>
//                           </FuseAnimate>
//                           <FuseAnimate animation="transition.slideLeftIn" delay={300}>
//                               <Typography className="md:ml-24" variant="h4" color="inherit">{call.userName}</Typography>
//                               {/*<Typography className="md:ml-24" variant="h6" color="inherit">{user.puesto}</Typography>*/}
//                           </FuseAnimate>
//                       </div>
//
//                       <div className="flex items-center justify-end">
//                           <Button className="mr-8 normal-case" variant="contained" color="secondary" aria-label="Follow">Follow</Button>
//                           <Button className="normal-case" variant="contained" color="primary" aria-label="Send Message">Send Message</Button>
//                       </div>
//                   </div>
//               }
//               contentToolbar={
//                   <Tabs
//                     value={selectedTab}
//                     onChange={handleTabChange}
//                     indicatorColor="primary"
//                     textColor="primary"
//                     variant="scrollable"
//                     scrollButtons="off"
//                     classes={{
//                         root: "h-64 w-full border-b-1"
//                     }}
//                   >
//                       <Tab
//                         classes={{
//                             root: "h-64"
//                         }}
//                         label="Timeline"/>
//                       <Tab
//                         classes={{
//                             root: "h-64"
//                         }} label="About"/>
//                       <Tab
//                         classes={{
//                             root: "h-64"
//                         }} label="Photos & Videos"/>
//                   </Tabs>
//               }
//               content={
//                   <div className="p-16 sm:p-24">
//                       {selectedTab === 0 &&
//                       (
//                         <TimelineTab/>
//                       )}
//                       {selectedTab === 1 && (
//                         <AboutTab/>
//                       )}
//                       {selectedTab === 2 && (
//                         <PhotosVideosTab/>
//                       )}
//                   </div>
//               }
//             />
//           ): null
//     )
// }
//
// export default ProfilePage;
