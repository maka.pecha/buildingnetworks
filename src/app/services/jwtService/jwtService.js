import axios from 'axios';
import jwtDecode from 'jwt-decode';
import FuseUtils from '@fuse/FuseUtils';

class jwtService extends FuseUtils.EventEmitter {

    init()
    {
        this.setInterceptors();
        this.handleAuthentication();
    }

    setInterceptors = () => {
        axios.interceptors.response.use(response => {
            return response;
        }, err => {
            return new Promise((resolve, reject) => {
                if ( err.response.status === 401 && err.config && !err.config.__isRetryRequest )
                {
                    // if you ever get an unauthorized response, logout the user
                    this.emit('onAutoLogout', 'Invalid access_token');
                    this.setSession(null);
                }
                throw err;
            });
        });
    };

    handleAuthentication = () => {

        let access_token = this.getAccessToken();

        if ( !access_token )
        {
            this.emit('onNoAccessToken');

            return;
        }

        if ( this.isAuthTokenValid(access_token) )
        {
            this.setSession(access_token);
            this.emit('onAutoLogin', true);
        }
        else
        {
            this.setSession(null);
            this.emit('onAutoLogout', 'Sesión caducada, inicie nuevamente');
        }
    };

    createUser = (data) => {
        return new Promise((resolve, reject) => {
            axios.post('/api/auth/register', data)
                .then(response => {
                    if ( response.data.user )
                    {
                        this.setSession(response.data.access_token);
                        resolve(response.data.user);
                    }
                    else
                    {
                        reject(response.data.error);
                    }
                });
        });
    };

    signInWithEmailAndPassword = (username, password) => {
        return new Promise((resolve, reject) => {
            axios.defaults.baseURL = 'http://salsipuedes.capitalinasdc.com';
            axios.post('/api/auth/login', {
                    username,
                    password
            }).then(response => {
                if ( response.status === 200)
                {
                    this.setSession(response.data.token);
                    if(response.data.token === null){
                        reject('Usuario o contraseña incorrecta')
                    }
                    else{
                        resolve(response.data.token);
                    }
                }
                else
                {
                    reject('Error en el servidor');
                }
            });
        });
    };

    signInWithToken = () => {
        return new Promise((resolve, reject) => {
            const access_token = this.getAccessToken();
            if(access_token){
                const myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");
                myHeaders.append("Authorization", `Bearer ${access_token}`);

                const requestOptions = {
                    method: 'GET',
                    headers: myHeaders,
                    redirect: 'follow'
                };

                fetch("http://salsipuedes.capitalinasdc.com/api/usuarios?minimalUser=true", requestOptions)
                  .then(response => response.json())
                  .then(result => resolve(result))
                  .catch(error => reject('No se encuentra cuenta de usuario activa'));
            }
            // axios.get('/api/auth/access-token', {
            //     data: {
            //         access_token: this.getAccessToken()
            //     }
            // })
            //     .then(response => {
            //         if ( response.data.user )
            //         {
            //             this.setSession(response.data.access_token);
            //             resolve(response.data.user);
            //         }
            //         else
            //         {
            //             this.logout();
            //             reject('Failed to login with token.');
            //         }
            //     })
            //     .catch(error => {
            //         this.logout();
            //         reject('Failed to login with token.');
            //     });
        });
    };

    updateUserData = (user) => {
        return axios.post('/api/auth/user/update', {
            user: user
        });
    };

    setSession = token => {
        if ( token )
        {
            localStorage.setItem('id_token', token);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        }
        else
        {
            localStorage.removeItem('id_token');
            delete axios.defaults.headers.common['Authorization'];
        }
    };

    logout = () => {
        this.setSession(null);
    };

    isAuthTokenValid = access_token => {
        if ( !access_token )
        {
            return false;
        }
        const decoded = jwtDecode(access_token);
        const currentTime = Date.now() / 1000;
        if ( decoded.exp < currentTime )
        {
            console.warn('access token expired');
            return false;
        }
        else
        {
            return true;
        }
    };

    getAccessToken = () => {
        return window.localStorage.getItem('id_token');
    };
}

const instance = new jwtService();

export default instance;
